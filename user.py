from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, get_raw_jwt, create_access_token, create_refresh_token, jwt_refresh_token_required, get_jwt_identity, fresh_jwt_required, decode_token
from user_model import UserModel
from blacklist import BLACKLIST
from flask_mail import Message
from email_service import mail
import hashlib
import datetime


class User(Resource):
    def get(self, user_id):
        user = UserModel.find_user_by_id(user_id)
        if user:
            return user.json()

        return {"message": "User not found!"}, 404

    @fresh_jwt_required
    def delete(self, user_id):
        user = UserModel.find_user_by_id(user_id)
        if user:
            user.remove_from_db()

            return {"message": "User deleted!"}, 200

        return {"message": "User not found!"}, 404


class UserRegister(Resource):
    def post(self):
        _user_parser = reqparse.RequestParser()
        _user_parser.add_argument(
            "username",
            type=str,
            required=True,
            help="This field cannot be blank"
        )
        _user_parser.add_argument(
            "password",
            type=str,
            required=True,
            help="This field cannot be blank"
        )

        _user_parser.add_argument(
            "device_id",
            type=str,
            required=True,
            help="This field cannot be blank"
        )

        data = _user_parser.parse_args()

        if UserModel.find_user_by_username(data["username"]):
            return {"message": "User exists!"}, 400

        user = UserModel(data["username"], hashlib.sha256(
            data["password"].encode("utf-8")).hexdigest(), data["device_id"])
        user.save_to_db()

        return {"message": f'Account successfully created for {data["username"]}'}


class UserLogin(Resource):
    def post(self):
        _user_parser = reqparse.RequestParser()
        _user_parser.add_argument(
            "username",
            type=str,
            required=True,
            help="This field cannot be blank"
        )
        _user_parser.add_argument(
            "password",
            type=str,
            required=True,
            help="This field cannot be blank"
        )

        _user_parser.add_argument(
            "device_id",
            type=str,
            required=True,
            help="This field cannot be blank"
        )

        data = _user_parser.parse_args()
        user = UserModel.find_user_by_username(data["username"])

        if user and user.password == hashlib.sha256(data["password"].encode("utf-8")).hexdigest() and user.device_id == data["device_id"]:
            access_token = create_access_token(
                identity=user.id, fresh=True, expires_delta=datetime.timedelta(minutes=30))
            refresh_token = create_refresh_token(identity=user.id)

            return {"access_token": access_token, "refresh_token": refresh_token}, 200

        return {"message": "Invalid credentials!"}, 401


class UserLogout(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        BLACKLIST.add(jti)
        return {"message": "Successfully logged out"}, 200


class UserForgotPassword(Resource):
    def send_email(self, subject, recipients, text_body):
        msg = Message(subject, recipients=recipients)
        msg.body = text_body
        mail.send(msg)

    def post(self):
        _user_parser = reqparse.RequestParser()
        _user_parser.add_argument(
            "username",
            type=str,
            required=True,
            help="This field cannot be blank"
        )

        data = _user_parser.parse_args()
        user = UserModel.find_user_by_username(data["username"])

        if user:
            reset_token = create_access_token(
                identity=user.id, fresh=True, expires_delta=datetime.timedelta(minutes=30))
            return self.send_email('[Flowerfy] Reset Your Password',
                                   recipients=[user.username],
                                   text_body=f'Your link to reset password. Link expires in 30 minutes  http://0.0.0.0:80/{reset_token}')

        return {"message": "User not found"}, 404


class UserResetPassword(Resource):
    def post(self):
        _user_parser = reqparse.RequestParser()
        _user_parser.add_argument(
            "new_password",
            type=str,
            required=True,
            help="This field cannot be blank"
        )
        _user_parser.add_argument(
            "confirm_password",
            type=str,
            required=True,
            help="This field cannot be blank"
        )

        _user_parser.add_argument(
            "reset_token",
            type=str,
            required=True,
            help="This field cannot be blank"
        )

        data = _user_parser.parse_args()
        user_id = decode_token(data["reset_token"])['identity']
        user = UserModel.find_user_by_id(user_id)

        if user and data["new_password"] == data["confirm_password"]:
            password = hashlib.sha256(
                data["new_password"].encode("utf-8")).hexdigest()
            user.password = password
            user.save_to_db()

            return {"message": "Password changed successfully"}, 200

        return {"message": "Something went wrong, Please check your credentials"}, 500


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user_id = get_jwt_identity()
        new_token = create_access_token(
            identity=current_user_id, fresh=False, expires_delta=datetime.timedelta(days=7))
        return {
            "access_token": new_token
        }, 200
