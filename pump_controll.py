from flask import Flask
from flask_restful import Resource
from flask_jwt_extended import jwt_required
import water

class Pump(Resource):
    @jwt_required
    def get(self):
        water.pump_on()


class Sensor(Resource):
    @jwt_required
    def get(self):
        status = water.get_status()
        message = ""
        if (status == 0):
            message = "Water me please!"
        else:
            message = "I'm a happy plant"
            
        return {"message": message}, 200