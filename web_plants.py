from flask import Flask, jsonify
from flask_restful import Api
from flask_jwt_extended import JWTManager
from user import User, UserRegister, UserLogin, UserLogout, UserForgotPassword, UserResetPassword, TokenRefresh
from pump_controll import Pump, Sensor
from blacklist import BLACKLIST
import datetime
import os

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = os.getenv(JWT_SECRET_KEY)
app.config['MAIL_SERVER'] ='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'flowerfy2020@gmail.com'
app.config['MAIL_PASSWORD'] = os.getenv(MAIL_PASSWORD)
app.config['MAIL_DEFAULT_SENDER'] = 'flowerfy2020@gmail.com'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///dbforuser.sqlite3'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config["PROPAGATE_EXCEPTIONS"] = True

api = Api(app)

jwt = JWTManager(app)

@jwt.expired_token_loader
def expired_token_callback():
    return jsonify(
        {
            "description": "Token has expired!",
            "error": "token_expired"
        }, 401
    )

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    return decrypted_token["jti"] in BLACKLIST

@jwt.invalid_token_loader
def invalid_token_callback():
    return jsonify(
        {
            "description": "Signature verification failed!",
            "error": "invalid_token"
        }, 401
    )

@jwt.unauthorized_loader
def unauthorized_loader_callback(error):
    return jsonify(
        {
            "description": "Access token not found!",
            "error": "unauthorized_loader"
        }, 401
    )

@jwt.needs_fresh_token_loader
def fresh_token_loader_callback():
    return jsonify(
        {
            "description": "Token is not fresh. Fresh token needed!",
            "error": "needs_fresh_token"
        }, 401
    )

api.add_resource(User, "/user/<int:user_id>")
api.add_resource(UserRegister, "/register")
api.add_resource(UserLogin, "/login")
api.add_resource(UserLogout, "/logout")
api.add_resource(UserForgotPassword, "/forgot_pwd")
api.add_resource(UserResetPassword, "/reset_pwd")
api.add_resource(TokenRefresh, "/refresh")
api.add_resource(Sensor, "/sensor")
api.add_resource(Pump, "/pump")

if __name__ == "__main__":
    from db import db
    from email_service import mail

    db.init_app(app)
    mail.init_app(app)

    @app.before_first_request
    def create_tables():
        db.create_all()

    app.run(host='0.0.0.0', port=80, debug=True)